var EscrowExchange = artifacts.require("EscrowExchange");
var Token = artifacts.require("White");

contract('EscrowExchange', function (accounts) {
    // It should return false on token validation because token is not added yet
    it("It should return false on token validation", function () {
        return EscrowExchange.deployed().then(function (instance) {
            return instance.isValidToken.call(0);
        }).then(function (valid) {
            assert.equal(valid, false, "It was true");
        });
    });
    //It should return 0 on order count because there is no order yet in contract
    it("It should return 0 on order count", function () {
        return EscrowExchange.deployed().then(function (instance) {
            return instance.getOrderCount.call();
        }).then(function (count) {
            assert.equal(count, 0, "not exactly 0");
        });
    });
    //It should return 0 which mean there is no order exist
    it("It should return 0 order Id  ", function () {
        return EscrowExchange.deployed().then(function (instance) {
            return instance.getOrder.call(0);
        }).then(function (order) {
            assert.equal(order[0].toString(), 0, "not exactly 0");
        });
    });
    // It should return 0 which mean there is no token exist
    it("It should return 0 token Id ", function () {
        return EscrowExchange.deployed().then(function (instance) {
            return instance.getToken.call(0);
        }).then(function (token) {
            assert.equal(token[0].toString(), 0, "not exactly 0");
        });
    });

    //it should add the token and return 0
    it("it should add the token and return 0", function () {
        return EscrowExchange.deployed().then(function (instance) {
            return instance.addNewToken('Bob', '0x4eee4559bd589b1cdfc419f0eed2ff9cbd47f439', 10, 18).then(function () {
                return instance.getToken.call(0);
            }).then(function (token) {
                assert.equal(token[0].toString(), '0', "the token was not added");
            });
        });
    });
    //it should return true if status changes to Open Sale
    it("it should return true if status changes to Open Sale", function () {
        return EscrowExchange.deployed().then(function (instance) {

            return instance.addNewToken('Bob', '0x4eee4559bd589b1cdfc419f0eed2ff9cbd47f439', 10, 18).then(function () {
                return instance.updateTokenStatusForSale.call(0);
            }).then(function (token) {
                assert.equal(token, true, "token not updated");
            });
        });
    });
    //it should return true if status changes to END sale
    it("it should return true if status changes to END sale", function () {
        return EscrowExchange.deployed().then(function (instance) {

            return instance.addNewToken('Bob', '0x4eee4559bd589b1cdfc419f0eed2ff9cbd47f439', 10, 18).then(function () {
                return instance.updateTokenStatusForSale.call(0);
            }).then(function (token) {
                assert.equal(token, true, "token not updated");
            });
        });
    });
    //"it should return token address
    it("it should return token address", function () {
        return EscrowExchange.deployed().then(function (instance) {

            return instance.addNewToken('Bob', '0x4eee4559bd589b1cdfc419f0eed2ff9cbd47f439', 10, 18).then(function () {
                return instance.getTokenAddressByID.call(0);
            }).then(function (token) {
                assert.equal(token, '0x4eee4559bd589b1cdfc419f0eed2ff9cbd47f439', "token not found");
            });
        });
    });
    //it should set fee rate 10
    it("it should set fee rate 10", function () {
        return EscrowExchange.deployed().then(function (instance) {

            return instance.setFeeRate(10).then(function () {
                return instance.feeRate.call();
            }).then(function (token) {
                assert.equal(token, 10, "Rate is not 10");
            });
        });
    });
    //it should edit token price and return true
    it("it should edit token price and return true", function () {
        return EscrowExchange.deployed().then(function (instance) {

            return instance.addNewToken('Bob', '0x4eee4559bd589b1cdfc419f0eed2ff9cbd47f439', 10, 18).then(function () {
                return instance.editTokenPrice.call(0, 10);
            }).then(function (token) {
                assert.equal(token, true, "Price is not set");
            });
        });
    });
    //it should set main address 0x4eee4559bd589b1cdfc419f0eed2ff9cbd47f439
    it("it should set main address 0x4eee4559bd589b1cdfc419f0eed2ff9cbd47f439", function () {
        return EscrowExchange.deployed().then(function (instance) {

            return instance.setMainAddress('0x4eee4559bd589b1cdfc419f0eed2ff9cbd47f439').then(function () {
                return instance.mainAddress.call();
            }).then(function (token) {
                assert.equal(token, '0x4eee4559bd589b1cdfc419f0eed2ff9cbd47f439', "new main address is not set");
            });
        });
    });

    //"it should update token A for sale
    it("it should update token A for sale", function () {
        return EscrowExchange.deployed().then(function (instance) {

            return instance.updateTokenStatusForSale(0).then(function (token) {
                let isValid;
                if (token.tx != null) {
                    isValid = true
                }
                assert.equal(isValid, true, "Token A or id of 0 is not updated for sale");
            });
        });
    });
    //"it should update token B for END sale
    it("it should update token B for END sale", function () {
        return EscrowExchange.deployed().then(function (instance) {

            return instance.updateTokenStatusToEndSale(1).then(function (token) {
                let isValid;
                if (token.tx != null) {
                    isValid = true
                }
                assert.equal(isValid, true, "Token B or id of 1 is not updated for end sale");
            });
        });
    });

    //it should place a order and return true. 
    it("it should return order placed true", async function () {
        return EscrowExchange.deployed().then(async function (instance) {
            return Token.deployed().then(function (tokenInstance) {
                return tokenInstance.approve(instance.address, 1000).then(function (token) {
                    return instance.addNewToken('Bill', tokenInstance.address, 1, 18).then(function (x) {

                        return instance.addNewToken('Bob', tokenInstance.address, 1, 18).then(function () {
                            return instance.updateTokenStatusForSale(5).then(function () {
                                return instance.updateTokenStatusForSale(6).then(function () {

                                    return instance.tradeToken(100, 5, 6, true, 1).then(function (token) {
                                        let isValid;
                                        if (token.tx != null) {
                                            isValid = true
                                        }
                                        assert.equal(isValid, true, "trade not happened");
                                    })
                                });

                            });
                        });
                    })
                })
            });
        });
    });

    //t should withdraw token and return true
    // it("it should withdraw token and return true", function () {
    //     return EscrowExchange.deployed().then(function (instance) {

    //         return instance.withdrawToken(0).then(function (token) {
    //             let isValid;
    //             if (token.tx != null) {
    //                 isValid = true
    //             }
    //             assert.equal(isValid, true, "Withdraw not happened");
    //         });
    //     });
    // });


});