var HDWalletProvider = require("truffle-hdwallet-provider");

var infura_apikey = "Lc2vdbhIswp6iQDRcmSa";
const mnemonic = 'shoe flavor fetch crawl try soup canyon lion fog fabric nuclear effort' // fs.readFileSync(".secret").toString().trim();
module.exports = {
  networks: {
    development: {
      host: "localhost",
      port: 8545,
      network_id: "*" // Match any network id
    }, //0x4eee4559bd589b1cdfc419f0eed2ff9cbd47f439
    rinkeby: {
      provider: new HDWalletProvider(mnemonic, "https://rinkeby.infura.io/"+infura_apikey),
      network_id: 4
    },
    ropsten: {
      provider: new HDWalletProvider(mnemonic, "https://ropsten.infura.io/"+infura_apikey),
      network_id: 3
    },
    solc: {
      optimizer: {
        enabled: true,
        runs: 200
      }
    }
  }
};