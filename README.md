ESCROW EXCHANGE TO LIQUIDATE TOKENS
Steps to use contract

1 - Owner Will add or list tokens with AddNewToken method.

2 - Owner will update token status for sale before user make trade with updateStatusForSale method.

3 - Owner will create the OrderContract based on the seller's requests. 

4 - Now user will make trade by calling tradeToken method.(depositTokenID, withdrawTokenId, TokenName, isSeller, contractID.)
Note: isSeller and contractId id optional means there is no use except adding them into orderbook.
Also before calling this method user has to approve funds from erc20 method of his deposit token approve method.

5 - Owner will withdraw tokens by calling withdrawToken method by adding orderID.

6 - Owner can update token status for End sale with updateStatusForSale method.

7 - Owner can editToken price.

8 - Owner can refund Token to depositer by adding order id.

9 - Owner can setMainAddress where all the fee will go.

10 - Owner can setFeeRate.

11 - Owner can transfer his ownership

You can visit that on https://rinkeby.etherscan.io/token/0xC9375A4aE2fb177FDa8E77fa3Cfa30bbcfDa357e#writeContract

% Audit Note
We don't need the liquidity function at this step since our Exchanges are linked into the orderContract. 
Inside our system, the token from the seller works as some kind of product. So If the buyer deposit the money into our escrow to buy that product, they only can buy that product(token) from the current seller.
