pragma solidity 0.4.24;


library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     * Counterpart to Solidity's `+` operator.
     * Requirements:
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, "SafeMath: addition overflow");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, "SafeMath: subtraction overflow");
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting with custom message on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     *
     * _Available since v2.4.0._
     */
    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b <= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-contracts/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, "SafeMath: multiplication overflow");

        return c;
    }

    /**
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     * Requirements:
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, "SafeMath: division by zero");
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts with custom message on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     *
     * _Available since v2.4.0._
     */
    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        // Solidity only automatically asserts when dividing by 0
        require(b > 0, errorMessage);
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        return mod(a, b, "SafeMath: modulo by zero");
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts with custom message when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     * Requirements:
     * - The divisor cannot be zero.
     *
     * _Available since v2.4.0._
     */
    function mod(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b != 0, errorMessage);
        return a % b;
    }
}

/**
 * General ERC20 interface 
 */
contract ERC20 {
    uint256 public totalSupply;
    uint256 public decimals;
    
    function balanceOf(address who) public view returns (uint256);
    function transfer(address to, uint256 value) public returns (bool);
    function approve(address spender, uint256 value) public returns (bool);
    function allowance(address owner, address spender) public view returns (uint256);
    function transferFrom(address from, address to, uint256 value) public returns (bool);

    event Approval(address indexed owner, address indexed spender, uint256 value);
    event Transfer(address indexed from, address indexed to, uint256 value);
}

/**
* Token Owner function
*/
contract Ownable {
  address private _owner;

  event OwnershipTransferred(
    address indexed previousOwner,
    address indexed newOwner
  );

  constructor() internal {
    _owner = msg.sender;
    emit OwnershipTransferred(address(0), _owner);
  }

  function owner() public view returns(address) {
    return _owner;
  }

  modifier onlyOwner() {
    require(isOwner());
    _;
  }

  function isOwner() public view returns(bool) {
    return msg.sender == _owner;
  }


 // It transfer the owner ship
  function transferOwnership(address newOwner) public onlyOwner {
    _transferOwnership(newOwner);
  }

  function _transferOwnership(address newOwner) internal {
    require(newOwner != address(0));
    emit OwnershipTransferred(_owner, newOwner);
    _owner = newOwner;
  }
}

/**
* This is the Main EscrowExchange function. All exchange is linked to the contract. OrderContract is create by main system and seperated 
* token and price. So we don't need any liquidity function at this time because our system is a sort of marketing system. 
* Not swap system. 
* 
*/
contract EscrowExchange is Ownable {
    
    uint public tokenCount;
    uint public orderCount;
    uint public contractCount;
    uint256 public feeRate;
    address public mainAddress;
    
    // To see the status of token
    enum TokenStatus {SaleNotStarted, OpenForSale, SaleEnded}

    // Order book struct where all trades stores. It will store data when tradeToken functions calls.
    struct OrderBook {
        uint orderID;
        address requesterAddress;
        ERC20 depositTokenAddress;
        ERC20 withdrawTokenAddress;
        bool isDeposited;
        bool isWithdrawn;
        bool isRefunded;
        bool isSeller;
        uint256 depositAmount;
        uint256 withdrawAmount;
        uint256 contractId; // all orderbook is linked to the ordercontract. 
        uint256 serviceFee; //store the fee when the orderbook is created
    }
    
    //OrderContract is to store the contract information 
    struct OrderContract{
        uint contractId;
        uint256 tokenPrice;
        uint tokenID;
        bool isFinished;
        bool isValidOrder;
    }
    
    // Save all tokens information. Token registeration triggered by addNewToken functions
    struct SharesToken {
        uint tokenID;
        ERC20 tokenAddress;
        string tokenName;
        TokenStatus tokenStatus;
        uint256 tokenPrice;
        uint256 decimals;
        bool isValidToken;
    }
    
    mapping(uint => SharesToken) public SharesTokenInfo;
    mapping(uint => OrderBook) public orderBook;
    mapping(uint => OrderContract) public OrderContracts;
    
    constructor (address _mainAddress) public {
        tokenCount = 0;
        contractCount = 0;
        orderCount = 0;
        feeRate = 3;
        mainAddress = _mainAddress;
    }    
    
    // Exchange Events
    event AddNewToken(uint tokenID, ERC20 tokenAddress, string tokenName, TokenStatus tokenStatus, uint256 tokenPrice);
    event AddOrderContract(uint contractId, uint256 tokenPrice, uint tokenID,bool isFinished);
    event UpdateTokenStatusForSale(uint TokenID, TokenStatus tokenStatus);
    event UpdateTokenStatusToEndSale(uint TokenID, TokenStatus tokenStatus);
    event UpdateOrderContractToEnd(uint contractId, bool isFinished);
    event DepositToken(uint orderID, address requesterAddress, string depositTokenName, string withdrawTokenName, ERC20 depositTokenAddress, ERC20 withdrawTokenAddress, uint256 depositAmount, uint256 withdrawAmount, bool isDeposited, bool isRefunded);
    event WithdrawToken(uint orderID,address requesterAddress,ERC20 depositTokenAddress,ERC20 withdrawTokenAddress,uint256 depositAmount, uint256 withdrawAmount, bool isDeposited, bool isWithdrawn,bool isRefunded);
    event RefundToken(uint orderID,address requesterAddress,ERC20 depositTokenAddress,ERC20 withdrawTokenAddress,uint256 depositAmount, uint256 withdrawAmount, bool isDeposited, bool isWithdrawn,bool isRefunded);
    event NewOrder(uint orderID, address requesterAddress, string depositTokenName, string withdrawTokenName, ERC20 depositTokenAddress, ERC20 withdrawTokenAddress, uint256 depositAmount, uint256 withdrawAmount, bool isDeposited, bool isWithdrawn,bool isRefunded);
    
    // It returns token balance in the contract by passing token ID
    function getBalanceOfToken(uint tokenID) public view returns (uint256) {
        return SharesTokenInfo[tokenID].tokenAddress.balanceOf(address(this));
    }
    
    // Admin can add new OrderContract 
    function addOrderContract (uint256 _tokenPrice, uint _tokenID) public onlyOwner returns (bool success) {
        
        require(!OrderContracts[contractCount].isValidOrder);
        
        OrderContracts[contractCount].contractId = contractCount;
        OrderContracts[contractCount].tokenPrice = _tokenPrice;
        OrderContracts[contractCount].tokenID = _tokenID;
        OrderContracts[contractCount].isFinished = false;
        OrderContracts[contractCount].isValidOrder = true;
        
        emit AddOrderContract(OrderContracts[contractCount].contractId,OrderContracts[contractCount].tokenPrice,OrderContracts[contractCount].tokenID,OrderContracts[contractCount].isFinished);
                
        contractCount++;
        return true;
    }
    
    // Admin can add new tokens 
    function addNewToken (string memory _tokenName, ERC20 _newTokenAddress, uint256 _tokenPrice) public onlyOwner returns (bool success) {
    
        require(!SharesTokenInfo[tokenCount].isValidToken);
        
        SharesTokenInfo[tokenCount].tokenID = tokenCount;
        SharesTokenInfo[tokenCount].tokenAddress = _newTokenAddress;
        SharesTokenInfo[tokenCount].tokenName = _tokenName;
        SharesTokenInfo[tokenCount].tokenStatus = TokenStatus.OpenForSale;
        SharesTokenInfo[tokenCount].tokenPrice = _tokenPrice;
        SharesTokenInfo[tokenCount].decimals = _newTokenAddress.decimals();
        SharesTokenInfo[tokenCount].isValidToken = true;

        emit AddNewToken(SharesTokenInfo[tokenCount].tokenID, SharesTokenInfo[tokenCount].tokenAddress, SharesTokenInfo[tokenCount].tokenName, SharesTokenInfo[tokenCount].tokenStatus, SharesTokenInfo[tokenCount].tokenPrice);
                
        tokenCount++;
        return true;
    }
    
    // Admin can update token status for sale of tokens
    function updateTokenStatusForSale (uint tokenID) public onlyOwner returns (bool success) {
        require(SharesTokenInfo[tokenID].isValidToken);
        require(SharesTokenInfo[tokenID].tokenStatus != TokenStatus.OpenForSale);

        SharesTokenInfo[tokenID].tokenStatus = TokenStatus.OpenForSale;
        
        emit UpdateTokenStatusForSale(tokenID, TokenStatus.OpenForSale);
        
        return true;
    }
    
    // Admin can update token status for sale of tokens
    function updateOrderContractToEnd (uint contractId) public onlyOwner returns (bool success) {
        require(OrderContracts[contractId].isValidOrder);
        
        //check if there is non-handle order on this orderContract
        bool status = true;
        for (uint i =0;i < orderCount; i++){
            if(orderBook[i].requesterAddress == msg.sender && orderBook[i].isRefunded == false && orderBook[i].isWithdrawn == false){
                status = false;
            }
        }
        
        if(status){
            OrderContracts[contractId].isFinished = true;
            emit UpdateOrderContractToEnd(contractId, true);
            return true;
        }else{
            return false;
        }
    
    }
    
    // Admin can update token status for End sale of tokens
    function updateTokenStatusToEndSale (uint tokenID) public onlyOwner returns (bool success) {
        require(SharesTokenInfo[tokenID].isValidToken);
        SharesTokenInfo[tokenID].tokenStatus = TokenStatus.SaleEnded;
        
        emit UpdateTokenStatusToEndSale(tokenID, TokenStatus.SaleEnded);
        
        return true;
    }
    
    // returns true if token is valid
    function isValidToken(uint tokenID) public view returns(bool success) {
        
        return SharesTokenInfo[tokenID].isValidToken;
    }
    
    // returns true if order is openstatus
    function isValidOrderContract(uint contractId) public view returns(bool success) {
        
        return OrderContracts[contractId].isValidOrder;
    }
    
    // returns tokens address    
    function getTokenAddressByID(uint tokenID) public view returns(ERC20 tokenAddress) {
        
        return SharesTokenInfo[tokenID].tokenAddress;
    }
    
    // Admin can set fee rate
    function setFeeRate (uint256 _rate) public onlyOwner {
        feeRate = _rate;
    }
    
    // admin can set new fee address of exchaneg
    function setMainAddress (address _address) public onlyOwner {
        mainAddress = _address;
    }
    
    // Users can trade tokens
    function tradeToken(uint depositTokenAmount, uint depositTokenID, uint withdrawTokenID, bool isSeller, uint256 contractId) public returns (uint orderID){
        require(isValidToken(depositTokenID));
        require(isValidToken(withdrawTokenID));
        require(isValidOrderContract(contractId));
        
        //require either depositID or withdrawID is same as contract TokenID
        require((OrderContracts[contractId].tokenID == depositTokenID && isSeller == true ) || (OrderContracts[contractId].tokenID == withdrawTokenID && isSeller == false ));
        require(OrderContracts[contractId].isFinished == false);
        require(SharesTokenInfo[depositTokenID].tokenStatus == TokenStatus.OpenForSale);
        require(SharesTokenInfo[withdrawTokenID].tokenStatus == TokenStatus.OpenForSale);
        
        uint orderTradeID = depositToken(getTokenAddressByID(depositTokenID), getTokenAddressByID(withdrawTokenID), depositTokenAmount, depositTokenID, withdrawTokenID, isSeller, contractId);
        
        emit NewOrder(orderTradeID, orderBook[orderTradeID].requesterAddress, SharesTokenInfo[depositTokenID].tokenName, SharesTokenInfo[withdrawTokenID].tokenName, orderBook[orderTradeID].depositTokenAddress, orderBook[orderTradeID].withdrawTokenAddress, orderBook[orderTradeID].depositAmount, orderBook[orderTradeID].withdrawAmount, orderBook[orderTradeID].isDeposited, orderBook[orderTradeID].isWithdrawn, orderBook[orderTradeID].isRefunded);
        return orderTradeID;
    }

    // internal function to depositToken
    function depositToken(ERC20 depositTokenAddress, ERC20 withdrawTokenAddress, uint depositTokenAmount, uint depositTokenID, uint withdrawTokenID, bool isSeller, uint256 contractId) internal returns (uint success) {
        require(isValidToken(depositTokenID));
        require(isValidToken(withdrawTokenID));
        require(isValidOrderContract(contractId));
        
        orderBook[orderCount].orderID = orderCount;
        orderBook[orderCount].requesterAddress = msg.sender;
        orderBook[orderCount].depositTokenAddress = depositTokenAddress;
        orderBook[orderCount].withdrawTokenAddress = withdrawTokenAddress;
        orderBook[orderCount].depositAmount = depositTokenAmount;
        orderBook[orderCount].isSeller = isSeller;
        orderBook[orderCount].contractId = contractId;
        orderBook[orderCount].serviceFee = feeRate;
        
        uint256 calcWithdrawTokens = 0;
        uint256 price_rate = 0;
        uint256 total_rate = 0;
        uint256 decimal_rate = SafeMath.div((10 ** SharesTokenInfo[withdrawTokenID].decimals),(10**SharesTokenInfo[depositTokenID].decimals));
        if(isSeller == false){
            
            price_rate = SafeMath.div(SharesTokenInfo[depositTokenID].tokenPrice,OrderContracts[contractId].tokenPrice);
            total_rate = SafeMath.mul(decimal_rate, price_rate);
            calcWithdrawTokens = SafeMath.mul(depositTokenAmount,total_rate);
            //calcWithdrawTokens = depositTokenAmount / (10 ** SharesTokenInfo[depositTokenID].decimals) * (10 ** SharesTokenInfo[withdrawTokenID].decimals) * SharesTokenInfo[depositTokenID].tokenPrice / (OrderContracts[contractId].tokenPrice);
            
        }else{

            price_rate = SafeMath.div(SharesTokenInfo[contractId].tokenPrice,OrderContracts[withdrawTokenID].tokenPrice);
            total_rate = SafeMath.mul(decimal_rate, price_rate);
            calcWithdrawTokens = SafeMath.mul(depositTokenAmount,total_rate);
            //calcWithdrawTokens = depositTokenAmount / (10 ** SharesTokenInfo[depositTokenID].decimals) * (10 ** SharesTokenInfo[withdrawTokenID].decimals) * OrderContracts[contractId].tokenPrice / (SharesTokenInfo[withdrawTokenID].tokenPrice);
        }
        
        orderBook[orderCount].withdrawAmount = calcWithdrawTokens;
        
        depositTokenAddress.transferFrom(msg.sender, address(this), orderBook[orderCount].depositAmount);
        
        orderBook[orderCount].isDeposited = true;
        
        emit DepositToken(orderBook[orderCount].orderID, orderBook[orderCount].requesterAddress, SharesTokenInfo[depositTokenID].tokenName, SharesTokenInfo[withdrawTokenID].tokenName, orderBook[orderCount].depositTokenAddress, orderBook[orderCount].withdrawTokenAddress, orderBook[orderCount].depositAmount, orderBook[orderCount].withdrawAmount, orderBook[orderCount].isDeposited, orderBook[orderCount].isRefunded);
                
        orderCount++;
        
        return orderBook[orderCount-1].orderID;
    }
    
    // withdrawToken function only called by admin to fulfill any trades
    function withdrawToken(uint orderID) public onlyOwner returns (bool success){

        require(orderBook[orderID].isDeposited);
        require(!orderBook[orderID].isWithdrawn);
        require(!orderBook[orderID].isRefunded);
        
        uint256 feeCalculate =  SafeMath.mul(SafeMath.div(orderBook[orderID].withdrawAmount,100),orderBook[orderID].serviceFee);
        uint256 totalWithdraw = SafeMath.sub(orderBook[orderID].withdrawAmount,feeCalculate);
        
        orderBook[orderID].withdrawTokenAddress.transfer(orderBook[orderID].requesterAddress, totalWithdraw);
        orderBook[orderID].withdrawTokenAddress.transfer(mainAddress, feeCalculate);
        orderBook[orderID].isWithdrawn = true;
        
        emit WithdrawToken(orderID, orderBook[orderID].requesterAddress, orderBook[orderID].depositTokenAddress, orderBook[orderID].withdrawTokenAddress, orderBook[orderID].depositAmount, orderBook[orderID].withdrawAmount, orderBook[orderCount].isDeposited, orderBook[orderCount].isWithdrawn,orderBook[orderCount].isRefunded);
        
        return true;
    
    }
    
    // Admin can edit token price 
    function editTokenPrice(uint tokenID, uint _tokenPrice) public onlyOwner returns(bool success) {
        
        SharesTokenInfo[tokenID].tokenPrice = _tokenPrice;
        return true;

    }
    
    // Admin can refund tokens to depositor 
    function refundToken (uint orderID) public onlyOwner returns (bool success) {
        require(orderBook[orderID].isDeposited);
        require(!orderBook[orderID].isWithdrawn);
        require(!orderBook[orderID].isRefunded);
        
        orderBook[orderID].depositTokenAddress.transfer(orderBook[orderID].requesterAddress, orderBook[orderID].depositAmount);
        orderBook[orderID].isRefunded = true;
        
        emit RefundToken(orderID, orderBook[orderID].requesterAddress, orderBook[orderID].depositTokenAddress, orderBook[orderID].withdrawTokenAddress, orderBook[orderID].depositAmount, orderBook[orderID].withdrawAmount, orderBook[orderCount].isDeposited, orderBook[orderCount].isWithdrawn,orderBook[orderCount].isRefunded);
        return true;
    } 
    
    // returns order book count
    function getOrderCount() public view returns(uint256 _count){
        return orderCount;
    }
    
    // returns order info
    function getOrder (uint orderID) public view returns (uint _orderID, address _requesterAddress, address _depositTokenAddress, 
    address _withdrawTokenAddress, uint _depositTokenAmount , bool _isSeller, uint _contractId) {
        _orderID = orderBook[orderID].orderID;
        _requesterAddress = orderBook[orderID].requesterAddress;
        _depositTokenAddress = orderBook[orderID].depositTokenAddress;
        _withdrawTokenAddress = orderBook[orderID].withdrawTokenAddress;
       _depositTokenAmount = orderBook[orderID].depositAmount;
        _isSeller = orderBook[orderID].isSeller;
        _contractId = orderBook[orderID].contractId;
    } 

    // returns order info
    function getOrderByAddress (address _address,uint depositTokenAmount,  uint256 contractId) public view returns (uint _orderID, address _requesterAddress, address _depositTokenAddress, 
    address _withdrawTokenAddress, uint _depositTokenAmount , bool _isSeller, uint _contractId) {

        uint orderID = 0;
        for (uint i =0;i < orderCount; i++){
            if(orderBook[i].requesterAddress == _address && orderBook[i].depositAmount == depositTokenAmount && orderBook[i].contractId == contractId){
                orderID = i;
            }
        }
        _orderID = orderBook[orderID].orderID;
        _requesterAddress = orderBook[orderID].requesterAddress;
        _depositTokenAddress = orderBook[orderID].depositTokenAddress;
        _withdrawTokenAddress = orderBook[orderID].withdrawTokenAddress;
        _depositTokenAmount = orderBook[orderID].depositAmount;
        _isSeller = orderBook[orderID].isSeller;
        _contractId = orderBook[orderID].contractId;
    }

    // returns order info
    function getToken(uint tokenID) public view returns (uint _tokenID, address _newTokenAddress, string _tokenName, 
     uint _tokenPrice ,uint _tokenDecimals, bool _isValidToken) {
        _tokenID = SharesTokenInfo[tokenID].tokenID;
        _newTokenAddress = SharesTokenInfo[tokenID].tokenAddress;
        _tokenName = SharesTokenInfo[tokenID].tokenName;
        _tokenPrice =  SharesTokenInfo[tokenID].tokenPrice;
        _tokenDecimals =  SharesTokenInfo[tokenID].decimals;
        _isValidToken = SharesTokenInfo[tokenID].isValidToken;
    }

    // returns orderContracts information
    function getOrderContracts(uint contractId) public view returns (uint _contractId, uint256 _tokenPrice, uint  _tokenID, 
     bool _isFinished, bool _isValidOrder) {
        _contractId = OrderContracts[contractId].contractId;
        _tokenPrice = OrderContracts[contractId].tokenPrice;
        _tokenID = OrderContracts[contractId].tokenID;
        _isFinished = OrderContracts[contractId].isFinished;
        _isValidOrder = OrderContracts[contractId].isValidOrder;
    }
}